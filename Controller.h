#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <GL/freeglut.h>
#include <iostream>
#include <string>
#include "AppController.h"
#include "Button.h"
#include "Box.h"
#include "Oval.h"
#include "Node.h"
#include "Diamond.h"

class Controller : public AppController {
    Button* boxButton;
    Button* deleteButton;
    Button* ovalButton;
    Button* diamondButton;
    Node* item;

public:
    Controller(){
        // Initialize your state variables 
        boxButton = new Button("Box", -0.9f, -0.7f);
        deleteButton = new Button("Delete", 0.5f, -0.7f);
        ovalButton = new Button("Oval", -0.5f, -0.7f);
        diamondButton = new Button("Diamond", -0.1f, -0.7f);

        item = nullptr;
    }

    void leftMouseDown( float x, float y ){
        if (y < -0.7){
            if (boxButton->contains(x, y)) {
                if (item) {
                    delete item;
                }
                item = new Box("BOX");
            }
            else if (deleteButton->contains(x, y)) {
                if (item) {
                    delete item;
                    item = nullptr;
                }
            }
            else if (ovalButton->contains(x, y)) {
                if (item) {
                    delete item;
                }
                item = new Oval("OVAL");
            }
            else if (diamondButton->contains(x, y)) {
                if (item) {
                    delete item;
                }
                item = new Diamond("DIAMOND");
            }
        }
        else{
            if (item) {
                item->deselect();

                if (item->contains(x, y)) {
                    item->select();
                }
            }
        }
    }

    void mouseMotion( float x, float y ) {
        // Handle mouse motion
    }

    void render() {
        // Render some graphics

        if (item) {
            item->draw();
        }
        
        boxButton->draw();
        deleteButton->draw();
        ovalButton->draw();
        diamondButton->draw();
    }

    ~Controller(){
        delete boxButton;
        delete deleteButton;
        delete ovalButton;
        delete diamondButton;
        delete item;
    }

};

#endif