#ifndef NODE_H
#define NODE_H

#include "Button.h"
#include <iostream>

class Node : public Button {
public:
    Node() {
        text = " NODE ";
    }

    Node (std::string text) {
        this->text = text;
        resize();
    }

    virtual void draw() = 0;

    bool isSelected() {
        return pressed;
    }

    void select() {
        pressed = true;
    }

    void deselect() {
        pressed = false;
    }

    std::string getText() {
        return text;
    }

    // setters
    void setX(float x) {
        this->x = x;
    }

    void setY(float y) {
        this->y = y;
    }

    float getX() {
        return x;
    }

    float getY() {
        return y;
    }

    void setText(std::string text) {
        this->text = text;
    }

    ~Node() {
        std::cout << "~Node()" << std::endl;
    }
};

#endif