#ifndef BOX_H
#define BOX_H

#include "Node.h"
#include <iostream>

class Box : public Node {
public:
    Box() : Node() {
        // 
    }

    Box (std::string text) : Node(text) {
        //
    }

    void draw() {
        resize();
        
        glColor3f(1.0f, 1.0f, 1.0f);

        if (pressed) {
            glColor3f(0.8f, 0.8f, 0.8f);
        }

        // selection fill
        glBegin(GL_POLYGON);
            glVertex2f(x, y);
            glVertex2f(x + w, y);
            glVertex2f(x + w, y - h);
            glVertex2f(x, y - h);
        glEnd();

        // outline
        glColor3f(0.0f, 0.0f, 0.0f);
        glBegin(GL_LINES);
            glVertex2f(x, y);
            glVertex2f(x + w, y);

            glVertex2f(x + w, y);
            glVertex2f(x + w, y - h);

            glVertex2f(x + w, y - h);
            glVertex2f(x, y - h);

            glVertex2f(x, y - h);
            glVertex2f(x, y);
        glEnd();

     
        glRasterPos2f(x + (padding / 2), y - (h / 2) - 0.025f);
        for (int i = 0; i < text.length(); i++) {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, text[i]);
        }
    }

    ~Box() {
        std::cout << "~Box()" << std::endl;
    }
};

#endif