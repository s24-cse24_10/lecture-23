#ifndef OVAL_H
#define OVAL_H

#include "Node.h"
#include <iostream>
#include <cmath>

class Oval : public Node {
public:
    Oval() : Node() {
        //
    }

    Oval (std::string text) : Node(text) {
        //
    }

    void draw() {
        resize();
        
        glColor3f(1.0f, 1.0f, 1.0f);

        if (pressed) {
            glColor3f(0.8f, 0.8f, 0.8f);
        }

        float inc = (2.0f * M_PI) / 60.0f;
        float a = w / 2.0f;
        float b = h / 2.0f;

        // selection fill
        glBegin(GL_POLYGON);
            for (float theta = 0; theta <= 2 * M_PI; theta += inc) {
                glVertex2f(a * cos(theta) + a, b * sin(theta) - b);
            }
        glEnd();

        // outline
        glColor3f(0.0f, 0.0f, 0.0f);
        glBegin(GL_LINES);
            for (float theta = 0; theta <= 2 * M_PI; theta += inc) {
                glVertex2f(a * cos(theta) + a, b * sin(theta) - b);
                glVertex2f(a * cos(theta + inc) + a, b * sin(theta + inc) - b);
            }
        glEnd();

     
        glRasterPos2f(x + (padding / 2), y - (h / 2) - 0.025f);
        for (int i = 0; i < text.length(); i++) {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, text[i]);
        }
    }

    ~Oval() {
        std::cout << "~Oval()" << std::endl;
    }
};

#endif