#ifndef BUTTON_H
#define BUTTON_H

#include <GL/freeglut.h>
#include <string>

class Button {
protected:
    std::string text;
    float x;
    float y;
    float w;
    float h;
    bool pressed;
    float padding;


    void resize() {
        float textWidth = 0.0f;
        for (int i = 0; i < text.length(); i++) {
            textWidth += glutBitmapWidth(GLUT_BITMAP_9_BY_15, text[i]);
        }
        textWidth = 2.0f * (textWidth / 400.0f);
        w = textWidth + padding;
    }

public:
    Button() {
        x = 0.0f;
        y = 0.0f;
        w = 0.4f;
        h = 0.2f;
        text = "Button";
        pressed = false;
        padding = 0.06f;
        resize();
    }

    Button(std::string text, float x, float y) {
        this->text = text;
        this->x = x;
        this->y = y;
        w = 0.4f;
        h = 0.2f;
        pressed = false;
        padding = 0.06f;
        resize();
    }

    virtual void draw() {
        resize();
        
        glColor3f(1.0f, 1.0f, 1.0f);

        if (pressed) {
            glColor3f(0.8f, 0.8f, 0.8f);
        }

        // selection fill
        glBegin(GL_POLYGON);
            glVertex2f(x, y);
            glVertex2f(x + w, y);
            glVertex2f(x + w, y - h);
            glVertex2f(x, y - h);
        glEnd();

        // outline
        glColor3f(0.0f, 0.0f, 0.0f);
        glBegin(GL_LINES);
            glVertex2f(x, y);
            glVertex2f(x + w, y);

            glVertex2f(x + w, y);
            glVertex2f(x + w, y - h);

            glVertex2f(x + w, y - h);
            glVertex2f(x, y - h);

            glVertex2f(x, y - h);
            glVertex2f(x, y);
        glEnd();

     
        glRasterPos2f(x + (padding / 2), y - (h / 2) - 0.025f);
        for (int i = 0; i < text.length(); i++) {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, text[i]);
        }
    }

    virtual bool contains(float mx, float my) {
        if (mx >= x && mx <= x + w && my <= y && my >= y - h) {
            return true;
        } else {
            return false;
        }
    }

    virtual ~Button() {
        // 
    }
};

#endif