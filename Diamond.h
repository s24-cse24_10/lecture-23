#ifndef DIAMOND_H
#define DIAMOND_H

#include "Node.h"
#include <iostream>

class Diamond : public Node {

public:
    Diamond() : Node() {
        // 
    }

    Diamond(std::string text) : Node(text) {
        //
    }

    void draw() {
        resize();
        
        glColor3f(1.0f, 1.0f, 1.0f);

        if (pressed) {
            glColor3f(0.8f, 0.8f, 0.8f);
        }

        // selection fill
        glBegin(GL_POLYGON);
            glVertex2f(x - padding, y - h / 2.0f);
            glVertex2f(x + w / 2.0f, y - h);
            glVertex2f(x + w + padding, y - h / 2.0f);
            glVertex2f(x + w / 2.0f, y);
        glEnd();

        // outline
        glColor3f(0.0f, 0.0f, 0.0f);
        glBegin(GL_LINES);
            glVertex2f(x - padding, y - h / 2.0f);
            glVertex2f(x + w / 2.0f, y - h);

            glVertex2f(x + w / 2.0f, y - h);
            glVertex2f(x + w + padding, y - h / 2.0f);

            glVertex2f(x + w + padding, y - h / 2.0f);
            glVertex2f(x + w / 2.0f, y);

            glVertex2f(x + w / 2.0f, y);
            glVertex2f(x - padding, y - h / 2.0f);
        glEnd();

     
        glRasterPos2f(x + (padding / 2), y - (h / 2) - 0.025f);
        for (int i = 0; i < text.length(); i++) {
            glutBitmapCharacter(GLUT_BITMAP_9_BY_15, text[i]);
        }
    }

    ~Diamond() {
        std::cout << "~Diamond()" << std::endl;
    }
};


#endif